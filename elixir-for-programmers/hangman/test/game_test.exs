defmodule GameTest do
    use ExUnit.Case

    alias Hangman.Game

    test "new_game returns structure" do
        game = Game.new_game()

        assert game.turns_left == 7
        assert game.game_status == :initializing
        assert length(game.letters) > 0
        assert Enum.all?(game.letters, fn x -> x =~ ~r/[a-z]/ end)
        assert Enum.all?(game.letters, &(&1 =~ ~r/[a-z]/))
        for x <- game.letters, do: assert x =~ ~r/[a-z]/
        
        # nutty character comparison
        Enum.map(game.letters, &(String.to_charlist(&1)))
        |> Enum.all?(&(assert &1 >= 'a' and &1 <= 'z'))
    end

    # doesn't this test know that the implementation of game is a map?
    test "state isn't changed for :won or :lost state" do
        for state <- [:won, :lost] do
            game = Game.new_game()
            |> Map.put(:game_status, state)
            assert {^game, _} = Game.make_move(game, "a")
        end
    end

    test "first occurrence of letter is not already guessed" do
        game = Game.new_game()
        {game, _} = Game.make_move(game, "a")
        assert game.game_status != :already_guessed
    end

    test "second occurrence of letter is not already guessed" do
        game = Game.new_game()
        {game, _} = Game.make_move(game, "a")
        assert game.game_status != :already_guessed
        {game, _} = Game.make_move(game, "a")
        assert game.game_status == :already_guessed
    end

    test "a good guess is recognized" do
        game = Game.new_game("wibble")
        {game, _} = Game.make_move(game, "w")
        assert game.game_status == :good_guess
        assert game.turns_left == 7
    end

    test "a guessed word is a won game" do
        game = Game.new_game("wibble")
        {game, _} = Game.make_move(game, "w")
        assert game.game_status == :good_guess
        assert game.turns_left == 7
        {game, _} = Game.make_move(game, "i")
        assert game.game_status == :good_guess
        assert game.turns_left == 7
        {game, _} = Game.make_move(game, "b")
        assert game.game_status == :good_guess
        assert game.turns_left == 7
        {game, _} = Game.make_move(game, "l")
        assert game.game_status == :good_guess
        assert game.turns_left == 7
        {game, _} = Game.make_move(game, "e")
        assert game.game_status == :won
        assert game.turns_left == 7
    end

    test "bad guess is recognized" do
        game = Game.new_game("wibble")
        {game, _} = Game.make_move(game, "a")
        assert game.game_status == :bad_guess
        assert game.turns_left == 6
    end

    test "lost game is recognized" do
        game = Game.new_game("wibble")
        {game, _} = Game.make_move(game, "a")
        assert game.game_status == :bad_guess
        assert game.turns_left == 6
        {game, _} = Game.make_move(game, "c")
        assert game.game_status == :bad_guess
        assert game.turns_left == 5
        {game, _} = Game.make_move(game, "d")
        assert game.game_status == :bad_guess
        assert game.turns_left == 4
        {game, _} = Game.make_move(game, "f")
        assert game.game_status == :bad_guess
        assert game.turns_left == 3
        {game, _} = Game.make_move(game, "g")
        assert game.game_status == :bad_guess
        assert game.turns_left == 2
        {game, _} = Game.make_move(game, "h")
        assert game.game_status == :bad_guess
        assert game.turns_left == 1
        {game, _} = Game.make_move(game, "j")
        assert game.game_status == :lost
    end

    #test "try using comprehensions" do
    #    moves = [
    #        {"w", :good_guess},
    #        {"i", :good_guess},
    #        {"b", :good_guess},
    #        {"l", :good_guess},
    #        {"e", :won},
    #    ]

    #    game = Game.new_game("wibble")

    #    # i think this doesn't work because "game" on the LHS is already bound, so it's not updated as the collection is iterated.  the match is just repeatedly true, i think.
    #    # real reason is that variables within comprehension are local only to comprehension, and the outer scope isn't touched.
    #    for {guess, state} <- moves do
    #        {game, _} = Game.make_move(game, guess)
    #        IO.puts "length of guesses:  #{Enum.count(game.guessed)}"
    #        assert game.game_status == state
    #    end
    #end

    test "try using Enum.reduce" do
        moves = [
            {"w", :good_guess},
            {"i", :good_guess},
            {"b", :good_guess},
            {"l", :good_guess},
            {"e", :won},
        ]

        game = Game.new_game("wibble")

        # i think enumerable is moves, and accumulator is game
        Enum.reduce(moves, game, fn {guess, state}, acc ->
            {updated_game, _} = Game.make_move(acc, guess)
            assert updated_game.game_status == state
            updated_game
        end)
    end

    test "multiple characters is an invalid guess" do
        game = Game.new_game("wibble")
        {game, _} = Game.make_move(game, "be")
        assert game.game_status == :invalid_guess
    end

    test "capital letter is an invalid guess" do
        game = Game.new_game("wibble")
        {game, _} = Game.make_move(game, "W")
        assert game.game_status == :invalid_guess
    end

    test "non-ascii letter is an invalid guess" do
        game = Game.new_game("wibble")
        {game, _} = Game.make_move(game, "é")
        assert game.game_status == :invalid_guess
    end

end
