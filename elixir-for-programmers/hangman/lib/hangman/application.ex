defmodule Hangman.Application do

    use Application

    def start(_type, _args) do
        IO.puts("Inside Hangman.Application.start()...")
        import Supervisor.Spec  # for worker()

        children = [
            worker(Hangman.Server, []),
        ]

        options = [
            name: Hangman.Supervisor,
            strategy: :simple_one_for_one,  # when supervisor created, it doesn't start any children, just remembers their specs that were provided in start() (to worker()).  then later we say "supervisor start child" and it'll kick off a supervised process underneath this supervisor.  that way we can have a pool of as many child processes as we want, and they'll be supervised.
        ]

        Supervisor.start_link(children, options)
    end

end
