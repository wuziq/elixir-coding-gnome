defmodule Hangman.Game do

    # creates named map structure with same name as module
    defstruct(
        turns_left: 7,
        game_status: :initializing,
        letters: [],
        guessed: MapSet.new(),
    )

    def new_game() do
        Dictionary.random_word()
        |> new_game()
    end

    def new_game(word) do
        %Hangman.Game{
            letters:  word |> String.codepoints
        }
    end

    def make_move(game = %{ game_status: state }, _guess) when state in [:won, :lost] do
        game
        |> return_with_tally()
    end

    def make_move(game, guess) do
        validate_guess(game, guess, String.to_charlist(guess))
        |> return_with_tally()
    end

    def submit_guess(game, guess) do
        already_guessed = MapSet.member?(game.guessed, guess)
        accept_move(game, guess, already_guessed)
    end

    def tally(game) do
        %{
            game_status: game.game_status,
            turns_left: game.turns_left,
            guessed: game.guessed,
            revealed: game.letters |> reveal_guessed(game.guessed),
        }
    end




    defp validate_guess(game, _guess, codepoints) when length(codepoints) > 1 do
        IO.puts "length of codepoints is #{length(codepoints)}"
        Map.put(game, :game_status, :invalid_guess)
    end

    defp validate_guess(game, _guess, codepoints) when hd(codepoints) not in ?a..?z do
        IO.puts "head of codepoints is #{hd(codepoints)}"
        Map.put(game, :game_status, :invalid_guess)
    end

    defp validate_guess(game, guess, _codepoints) do
        submit_guess(game, guess)
    end

    defp accept_move(game, _guess, _already_guessed = true) do
        Map.put(game, :game_status, :already_guessed)
    end

    defp accept_move(game, guess, _not_already_guessed) do
        game = Map.put(game, :guessed, MapSet.put(game.guessed, guess))
        good_guess = Enum.member?(game.letters, guess)
        score_guess(game, good_guess)
    end

    defp score_guess(game, _good_guess = true) do
        new_state = MapSet.new(game.letters)
        |> MapSet.subset?(game.guessed)
        |> maybe_won()
        Map.put(game, :game_status, new_state)
    end

    defp score_guess(game = %{ turns_left: 1 }, _not_good_guess) do
        Map.put(game, :game_status, :lost)
    end

    defp score_guess(game = %{ turns_left: turns_left }, _not_good_guess) do
        #Map.put(game, :turns_left, turns_left - 1)
        #|> Map.put(:game_status, :bad_guess)

        %{ game | 
           turns_left: turns_left - 1, 
           game_status: :bad_guess }
    end

    defp maybe_won(true), do: :won
    defp maybe_won(_), do: :good_guess

    # iterate through each letter in the answer and determine whether it's been guessed or not
    defp reveal_guessed(letters, guessed) do
        letters
        |> Enum.map(fn letter -> reveal_letter(letter, MapSet.member?(guessed, letter)) end)
    end

    defp reveal_letter(letter, _in_word = true), do: letter
    defp reveal_letter(_letter, _not_in_word), do: "_"

    defp return_with_tally(game), do: {game, tally(game)}

end
