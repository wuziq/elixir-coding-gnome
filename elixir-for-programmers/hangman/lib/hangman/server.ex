defmodule Hangman.Server do

    alias Hangman.Game

    use GenServer  # adds GenServer behavior.  predefines a bunch of default implementations for various callbacks so we fewer to define ourselves.

    # respond to typical callback of "i want to start this as a separate process"
    def start_link() do
        # kick off GenServer process
        GenServer.start_link(__MODULE__, nil)  # nil cuz no args

        # now other callbacks will be called in the context of that new process, like init()
    end

    # returns initial state that is managed by this server going forward
    def init(_) do  # _ cuz we're not passing any args in start_link()
        # in our case, there's no args, and the state is simply the game we're creating
        IO.puts("Starting new game...")
        {:ok, Game.new_game()}
    end

    # _from is the calling process, and it's not used unless we're making async calls
    def handle_call({:make_move, guess}, _from, game_state) do
        {game_state, tally} = Game.make_move(game_state, guess)
        {:reply, tally, game_state}  # tally is what's returned to GenServer.call, and game_state is the updated state being maintained by this server
    end

    def handle_call({:tally}, _from, game_state) do
        {:reply, Game.tally(game_state), game_state}
    end

end
