defmodule GallowsWeb.HangmanController do
  use GallowsWeb, :controller

  def new_game(conn, _params) do
    render(conn, "new_game.html")
  end

  def create_game(conn, _params) do
    game = Hangman.new_game()
    tally = Hangman.tally(game)
    my_assigns = %{tally: tally}
    conn
    |> put_session(:game, game)
    |> render("game_field.html", my_assigns)
  end

  def make_move(conn, params) do
    # raise inspect(params)  # use this to see in the browser what params contains
    guess = params["make_move"]["guess"]
    tally = 
        conn
        |> get_session(:game)
        |> Hangman.make_move(guess)
    put_in(conn.params["make_move"]["guess"], "")
    |> render("game_field.html", tally: tally)
  end
end
