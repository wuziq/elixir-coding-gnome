defmodule GallowsWeb.HangmanView do
  use GallowsWeb, :view

  def word_so_far(tally) do
    tally.revealed |> Enum.join(" ")
  end

end
