defmodule Cache do

    alias Cache.Cache

    defdelegate add(num, fib_value), to: Cache
    defdelegate get(num), to: Cache

end
