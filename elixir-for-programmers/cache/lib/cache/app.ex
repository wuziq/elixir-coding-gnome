defmodule Cache.App do

    use Application

    def start(_type, _args) do
        Cache.Cache.start_link()
    end

end
