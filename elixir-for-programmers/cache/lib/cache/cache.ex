defmodule Cache.Cache do

    # things cache should do:
    # get existing fib value given a number
    # add fib values for a given number
    # should this be done by cache?  or by api to cache?  add value if it doesn't exist

    @me __MODULE__

    def start_link() do
        Agent.start_link(fn -> %{0 => 0, 1 => 1} end, name: @me)
    end

    def add(num, fib_value) do
        _ = Agent.update(@me, fn(cache_state) -> Map.put(cache_state, num, fib_value) end)
    end

    def get(num) do
        Agent.get(@me, fn(cache_state) -> Map.get(cache_state, num) end)
    end

end
