defmodule TextClient.Prompter do

    alias TextClient.State

    def prompt(client_state = %State{}) do
        IO.gets("Your guess:  ")
        |> check_input(client_state)
    end


    defp check_input({:error, reason}, _) do
        IO.puts("Game ended: #{reason}")
        exit(:normal)
    end

    defp check_input(:eof, _) do
        IO.puts("Looks like you gave up...")
        exit(:normal)
    end

    # it's weird for a "prompter" to modify state, isn't it?
    defp check_input(input, client_state = %State{}) do
        input = String.trim(input)
        cond do
            input =~ ~r/\A[a-z]\z/ ->
                Map.put(client_state, :guess, input)
            true ->  # match anything else
                IO.puts("Please enter a single lowercase letter")
                prompt(client_state)
        end
    end

end
