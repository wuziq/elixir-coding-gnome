defmodule TextClient.Mover do
   
    alias TextClient.State

    def make_move(client_state) do
        tally = Hangman.make_move(client_state.game_service, client_state.guess)
        %State{client_state | tally: tally}
    end

end
