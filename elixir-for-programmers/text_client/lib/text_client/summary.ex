defmodule TextClient.Summary do

    def display(client_state = %{tally: tally}) do
        IO.puts [
            "\n",
            "Word so far:  #{Enum.join(tally.revealed, " ")}\n",
            "Guessed letters:  #{Enum.join(tally.guessed, " ")}\n",
            "Guesses left:  #{tally.turns_left}\n",
        ]
        client_state
    end

end
