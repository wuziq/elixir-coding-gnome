defmodule TextClient.Interact do

    @hangman_server :"hangman@wuziqs-MacBook-Pro"  # just assume we'll start our iex with this name

    alias TextClient.{Player, State}

    def start() do
        new_game()
        |> set_up_state()
        |> Player.play()
    end

    #def play(state) do
    #    # gradually mutate state with each interact()
    #    # state = interact(state)
    #    # state = interact(state)
    #    # state = interact(state)
    #    play(state) # pass in new state.  state is maintained and updated each time around.
    #end

    # set up client state
    defp set_up_state(game_state) do
        %State{
            game_service: game_state,
            tally: Hangman.tally(game_state),
        }
    end

    defp new_game() do
        :rpc.call(@hangman_server, Hangman, :new_game, [])
    end

end
