defmodule TextClient.State do

    defstruct(
        # why is this called a service?  isn't it the game state?
        game_service: nil,
        tally: nil,
        guess: ""
    )

end
