defmodule TextClient.Player do

    alias TextClient.{Mover, Prompter, State, Summary}

    # won, lost, good guess, bad guess, already used, invalid input, initializing
    def play(%State{tally: %{game_status: :won}}) do
        exit_with_message("You WON!")
    end

    def play(%State{tally: %{game_status: :lost}}) do
        exit_with_message("Sorry, you lost :(")
    end

    def play(client_state = %State{tally: %{game_status: :good_guess}}) do
        continue_with_message(client_state, "Good guess!")
    end

    def play(client_state = %State{tally: %{game_status: :bad_guess}}) do
        continue_with_message(client_state, "Sorry, that isn't in the word.")
    end

    def play(client_state = %State{tally: %{game_status: :already_guessed}}) do
        continue_with_message(client_state, "You've already used that letter.")
    end

    def play(client_state) do
        continue(client_state)
    end

    def continue(client_state) do
        client_state
        |> Summary.display()
        |> Prompter.prompt()
        |> Mover.make_move()
        |> play()
    end

    defp continue_with_message(client_state, message) do
        IO.puts(message)
        continue(client_state)
    end

    defp exit_with_message(message) do
        IO.puts(message)
        exit(:normal)
    end

end
