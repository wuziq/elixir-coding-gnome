shopping = [
    { "1 dozen", "eggs" },
    { "1 ripe", "melon" },
    { "4", "apples" },
    { "2 boxes", "tea" },
]

template = """
quantity | item
-------------------
<%= for {quantity, item} <- list do %>
<%= String.pad_leading(quantity, String.length("quantity")) %> | <%= item %>
<% end %>
"""

IO.puts EEx.eval_string(template, [list: shopping], trim: true) # why doesn't the trim do anything?
