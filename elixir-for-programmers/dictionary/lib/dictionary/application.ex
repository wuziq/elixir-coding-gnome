defmodule Dictionary.Application do

    use Application

    def start(_type, _args) do
        IO.puts("Inside Dictionary.Application.start()...")
        import Supervisor.Spec  # for worker()

        children = [
            worker(Dictionary.WordList, []),  # i guess start_link() is expected to be implemented
        ]

        options = [
            name: Dictionary.Supervisor,
            strategy: :one_for_one,
        ]

        Supervisor.start_link(children, options)
    end

end
