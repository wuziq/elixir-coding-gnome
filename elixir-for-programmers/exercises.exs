# interpolate UTC time into a string
time = Time.utc_now
IO.puts ~s"#{time}"

# Q:  when is the interpolated expression evaluated?
# A:  when the interpolating line is hit, i guess?

# output instructions on how to interpolate expressions into strings
IO.puts ~S"For example, 1 + 2 = #{ 1 + 2 }"

# transform "now is the time" into a list
to_list = "now is the time"
for word <- ~w/#{to_list}/, do: IO.puts word

# regex matching
str = "once upon a time"
IO.puts ~S[does ~r/u..n/ match "] <> str <> ~S/"?/
(str =~ ~r/u..n/)
|> IO.puts

# return if string contains a, followed by any character, followed by c
IO.puts Regex.match?(~r/a.c/, "abc")
IO.puts Regex.match?(~r/a.c/, "ace")
to_replace = "the cat meowed at another cat"
IO.puts Regex.replace(~r/cat/, to_replace, "dog")
IO.puts Regex.replace(~r/cat/, to_replace, "dog", global: false)

# lists are EITHER:
# an empty list:  [ ]
# a value followed by a list:  [ value | list ]
# NOT A PROPER LIST:  [ value | value ]
# NOT A PROPER LIST:  [ list | value ]
# is a list, for some reason?:  [ list | list ]
# is this a bug?  is_list() returns true for the above improper lists.  no, not a bug.  erlang standard functions expect proper lists, which end with an empty list as their last cell.
# just remember this pipe format means [head | tail].  different from [a, b, c].  the tail is [b, c].
# note that in the [head | tail] pattern match, the head must receive a value (head can't be empty).
# also, the head part of the match can have multiple elements:  [ head1, head2 | tail ]

# pattern matching
# LHS and RHS must have same shape.
# if the thing on the left is an unbound variable, Elixir binds that variable to the value being matched.
# use pattern matching to fail early:  {:ok, file} = File.open("some/file")  # in this case, failure to open would return {:error, reason}, and the atoms :ok and :error don't match.
# all variables on the LHS are unbound before a match is made.  to keep a variable bound, prefix it with a caret, which is the pin operator.

# pattern matching function calls
# in functions, args aren't just assigned to params.  rather, each arg is pattern matched to its param.  so you can have a constant as a parameter.  so you can have a constant as a parameter.
# because of this, you can have an overloaded function that handles multiple scenarios, and you can call that function without explicit conditionals.  neat, but not fully convinced it's easier to read/debug/change.

defmodule Exercises do

    def swap({a, b}), do: {b, a}
    def matches(a, a), do: true
    def matches(_, _), do: false

end

IO.puts "Swapping {2, 5}..."
IO.inspect Exercises.swap({2, 5})
IO.puts Exercises.matches(1, 1)
IO.puts Exercises.matches(1, 2)

defmodule Lists do

  def len([]), do: 0
  def len([_h|t]), do: 1 + len(t)

  def sum([]), do: 0
  def sum([h|t]), do: h + sum(t)

  def double([]), do: []
  def double([h|t]), do: [ 2*h | double(t) ]

  def square([]), do: []
  def square([h|t]), do: [ h*h | square(t) ]

  def sum_pairs([]), do: []
  def sum_pairs([h1, h2 | t]), do: [ h1+h2 | sum_pairs(t) ]

  def even_length?([]), do: true
  def even_length?([_h1, _h2 | t]), do: even_length?(t)
  def even_length?([_h|_t]), do: false

end


# writing the game
# every function body is responsible for only one transformation
# top level lib module is api.  implementation lives in subdirectories under lib.
# implementation module names use convention of Toplevelmodule.Module

# tests
# file must end in _test.exs
# convention is that prefix is name of module
# why test API?
# 1.  coupling tests to implementation details yields brittle tests
# 2.  API is what clients actually call so we want to assert behavior the client sees
# 3.  testing API layer is more of an integration test, which provides good assurance for happy paths.  not sure how much it applies here, though, since API layer is thin delegation layer.
# why test implementation?
# 1.  implementation is just another layer and so should be tested
# 2.  enables us to test individual units, which helps us to narrow down source of problems
# 3.  API layer is just a thin delegation layer, does no work, whereas implementation does all work


# writing game
# recall that map keys can be atoms.
# apparently there's no ordered set
# use "mix test --trace" to get more verbose output from test, helpful to know which test is running

# comprehensions
# result = for pattern <- collection, do
# where there can be multiple pattern-collection clauses.
# if there is any non-pattern-collection clause, it's treated as a predicate.  if predicate returns false, current iteration is bypassed, like continue.
# for the last two examples, if x is 5, there's no value between 6-9 that could be added to yield 10.
# more important is the fact the the first example still iterates through all possibilities, whereas the last example's set of possibilities is constrained.
# use :into to add comprehension results into a collection of your choosing.
# also:  :math.pow(x,2) is much much slower than x*x
# use :timer.tc(some function) to measure execution time in microseconds
# i think the comprehension's body is a closure.  it is closed at the time of creation, and modifications to outside variables just happen to a local copy.

# you can pattern match into an argument's nested value, or extract that value into a variable (as we did with "when" in the function signature)

# text client
# whenever we want to package state together, strongly recommend using a struct
# if you put IO.inspect() in a pipeline, it will output its parameter in a human-readable form, but it ALSO returns whatever was passed to it.  nice!  so you can keep passing through the pipeline.
# try to divide code into modules, where each is responsible for one aspect of the overall app.
# within each module, try to write functions that perform just one transformation of state.
# put shared state into its own module.
# IO.puts can accept a list
# recall the map updating syntax with pipe, which is useful when you want to update more than one thing in a map:  %SomeMap{map_to_update | key1: new_value1, key2: new_value2 }

# processes
# use spawn to start a new process
# recall anon functions can be multiple lines separated with semicolon
# e.g., greeter = fn -> Process.sleep(1000); IO.puts("hello") end
# can spawn with anon function, or with module, function name, and function args
# spawn(fn -> IO.puts "hello" end)
# spawn(Procs, :greeter, ["world"])
# passing module, function, and args is very common, and known as MFA (module, function, args)
# note that we almost never use Process.sleep in elixir
# to communicate with a process, use send() and receive() to pass messages
# spawn returns PID, which becomes stale when process ends.
# if want to keep listening, just call receive again
# messages sent to nonexistent processes are silently thrown away
# can also pattern match on received messages.  useful for updating state in the process, for example.
# should all messages be wrapped in tuples?  would be good for consistency, because pattern matching on a single atom won't match a single atom in a tuple.
# spawn'd processes are totally independent from the starting process.  to link them, use spawn_link.  linking them will cause the starting process to be killed if the spawned process dies abnormally, and vice versa.  the killed linked process is then restarted.  key point:  ABNORMAL exits.  for normal exits, the linked process isn't killed.
# the restarting of the killed linked process is core reliable system behavior of elixir.  to be seen more with supervision trees.
# get into the habit of linking created processes.  that way we won't leave random processes lying around if things go wrong.

# agents
# background process maintains state, then tell process to run some function on that state.
# they're useful for storing background state.
# but don't use agents as objects, if you're used to OOP.
# Agent.start_link(func):  func will initialize the agent's state.
# Agent.get(pid, func):  run func in the agent with agent's state.  whatever func returns will be what Agent.get returns.
# Agent.update(pid, func):  run func in the agent with agent's state.  whatever func returns will become the agent's new state.
# Agent.get_and_update(pid, func):  run func in the agent with agent's state.  func should return a two-element tuple:  first element is current state, and second element is the agent's updated state.
# since agents are easy to abuse, it's important to wrap them in modules, exposing their usage through module APIs instead, such that the agent is just an implementation detail.
# whenever you want to "keep something lying around", you should probably think of using an agent.
# recall that if an exception is thrown by the agent, the calling process also exits.

# applications
# libraries use their client's state, whereas application has its own state.
# in elixir, in order to have state, we need a process.
# library code runs in client's state.  application code runs in its own process.
# i guess library is typically what i already know of a library.  no life of its own.  and you can have projects that are libraries or applications.
# we define "main" in elixir by using the Application behavior:  "use Application".  then by implementing start(_type, _args).  the elixir runtime will create a root process via start() (which is why we don't want to use start_link(), since if the started process fails, so will the whole elixir runtime).  and then the started process can start other processes if desired.
# _type and _args are basically never used.
# we also need to let the runtime know where our main is, and we do that in the mix.exs file, under the "application" callback.
# the "application" callback is normally used to register other application that our application uses.  but it's also where you declare where your main is.
# in order to hide an agent under your api, you can name the agent's process, passing in an atom via its start_link.  nice!  convention for names is to use the module name, or the macro __MODULE__
# speaking of, we can define a constant using @.  it's a module attribute.
# a node is a running copy of the elixir runtime.  by default, each node runs in isolation, but we can connect multiple nodes into a mesh.
# restarted processes will have different PID but the same name!

# supervisors
# supervisors look after processes and other supervisors, forming a supervision tree.
# supervision tree have nothing to do with process tree.
# structure supervisors to maximize their chances of keeping processes up and running (?)
# supervisors sit outside regular process structure
# supervisors need to start PRIOR to any process it monitors.  so in our case, it should process before app start?
# you can set params on a supervisor that tell it to not restart if it crashes so many times in so many seconds.
# supervisor is a separate concern from your app.  an API talks to processes that the supervisor monitors.
# supervisors are just another process.

# apparently you can reference a library using an atom

# each client talks to a game process in a pool of processes.  that process IS the state for that game.
# typically in erlang/elixir is to have a process for each client, it's created when we get a connection from a client, and then the process is destroyed when we lose the client.  gives some resilience, code that's easier to understand.
# the processes in that pool are called servers.  server has a specific meaning.  agents are actually implemented using this particular server technology called GenServer.
# GenServer is part of erlang OTP framework and as such is part of the reason erlang exists.
# GenServer has two sets of APIs:  an external API and internal callbacks
# external API runs in the calling process
# internal callbacks run in server process
# GenServer handles state that we've normally seen get passed to and from a client, all under its covers.
# keep actual implementation separate from the code that is the server.  the server is invoked from API and then delegates processing to the implementation.
# also keep API separate from the server.
# beware that this separation of concerns is not normally done by elixir coders, which can lead to big god modules.
# so the 3 separate concerns of API, server, and implementation should be split into 3 modules.  this seems way cleaner to me anyway.
# GenServer.call is an implementation detail that the API can invoke.
# the :simple_one_for_one strategy is perfect for creating dynamic pools of the same server.  when the supervisor is initialized with it, it doesn't create any server processes right away.  you need to call Supervisor.start_child to do that.
# recall that a way to start a supervisor is via an application.  so create application module per usual and import Supervisor.Spec as before.  or if starting from scratch, can achieve this via "mix new hangman --sup"

# i'm guessing it's the server that should be supervised.  genservers maintain state, so i'm wondering whether that state is somehow recreated or preserved if a supervisor restarts the process owning the state.  but the PID will be different, do i need to use named processes somehow?

# nodes
# a node is an instance of the elixir/erlang runtime
# by defualt, each node is isolated from every other node.
# but nodes with names may interconnect
# short name:  <some name>@<first part of hostname>, e.g., fred@quarry
# long name:  <some name>@<fully qualified hostname>, e.g., fred@quarry.bedrock.com
# short names are used for nodes on the same computer, whereas long names are for networked nodes
# can't mix short and long names.
# "iex" or "mix" starts up a node.
# provide name on command line:  iex --sname fred  OR  iex --name fred@quarry.bedrock.com.  long name is --name not --sname.  hostname is guessed for the short name.
# nodes with names will try to connect the first time one node tries to reference another node.  can do this manually:  Node.connect(:fred@quarry).  once done, connection is BIDIRECTIONAL.  Node.list() to see all nodes we're connected to.
# node connections are transitive, so everything connects to everything else.  we can arrange it so that this doesn't happen though.
# with elixir, it's pretty much location transparent, so anything that can be done on one node can be done on a remote node as well:  process creation, linking, monitoring, message passing

# process identifiers
# #PID<a.b.c> where bottom two numbers b and c represent actual pid in this environment, where the top number a is the node.
# local PIDs:  a is always 0
# can also register a name with a process, like :chatbot.  use Process.register(pid, :chatbot).  then send(pid, :hello) or send(:chatbot, :hello).
# registered names are local to the node that registered it.  so other nodes need to use not just the name, but also the name of the node:  send({:pidname, :othernode@otherhost}, "foo")
# so if we want to go to remote node, can use name and then node id:  {:chatbot, :n1@farm.com}.  so send({:chatbot, :n1@farm.com}, :hello)
# if a node sends a pid to another node, then that PID is automatically translated on the way out, so our a becomes non-zero.  if we send to that updated pid, that message is automatically routed to the original node.

# a node isn't considered to be on a network if it doesn't have a name.

# to run code on another node, we need to spawn a process on that other node:  Node.spawn().  but you don't really want to do this in real-life code - you're going to be using OTP and GenServers instead, cuz those frameworks do things like error handling and timeouts and node recoveries.  Node.spawn() is very low level so don't use it unless you really know what you're doing.
# note that you can spawn a process locally using just spawn() instead of Node.spawn().  i wonder whether they're the same thing, though.
# processes run under control of a group leader.  group leader runs separate processes for various stdin and stdout.  an iostream in elixir isn't a file handle - it's a pid!  if you send an appropriately-formmatted message to that pid, it'll do the necessary IO.
# when a process is spawned, it's spawned with a group leader of the calling node, so any IO done on the spawned process is going back to the calling node's group leader.  pretty cool.

# elixir message passing is strictly one way.  there's no reply function.  if you want to reply, you need to tell the server where to send a message back.

# when connecting to other computers, don't forget to use full names, and you also need to provide an agreed-upon value to --cookie:
# iex --name macbook@192.168.0.146 --cookie dough
# iex --name livingroom@192.168.0.198 --cookie dough
# if --cookie isn't provided, then the runtime looks for .erlang.cookie in your home directory.  if it's not found, it'll create one and stick a random string in it.  so, locally, we are actually using cookies, we just don't specify them explicitly.

# TRAFFIC BETWEEN ELIXIR NODES IS CLEAR.  traffic should be tunneled through SSH or VPN.

# if sending to a named process that doesn't exist, you get an argument error.  interesting.

# ELIXIR AUTOMATICALLY STARTS ANY DEPENDENCY THAT IS ALSO AN APPLICATION.
# to only include and load the code but NOT start it, use "included_applications: [ :yourappdep ]" in mix.exs application()
# BUT.  this technique is rarely used cuz we'd instead be using frameworks to manage this for us.  it just goes to show how code and processes are loosely related in elixir.
# we do this exercise to not only enable hangman servers to run on a separate node, but also to make us think about where the state lies (in hangman server) and where the code runs (some in server, some in client).
# so now, how does client gain initial access to the new hangman server, assuming the server is running on some other node?  the client would have to tell the supervisor in the hangman app to create a new server for it.

# phoenix
# phoenix is not our application.  so why stick it all into a big lump when we can keep it separate?
# "mix archive" to see what archives are installed and where.
# no need for database layer (so, --no-ecto) cuz phoenix is purely concerned with communicating on web, and anything that uses a database should be built into a service, like what we did with hangman and dictionary.
# brunch is used for asset management
# phoenix bundles all of its dependencies inside each app it creates.  so there's a ton of files and folders.
# it might be tempting to include elixir code alongside phoenix code directly or as an umbrella app.  fight this temptation to stick everything in the phoenix app.
# keep js and css under assets
# like other web frameworks with templating systems, the html template will have code that's marked for substitutions.  in phoenix, those are marked with <%= some elixir code %>
# by default, phoenix will autoreload if it detects any changes to files.
# router.ex tells phoenix how an incoming request gets mapped to executable code.
# all functions in controllers take a connection parameter, which contains all details of the incoming request.
# as we proces the request, we actually augment that connection argument.  so it's not just the state of the request, it's also the state of the processing of the request.  more and more info gets added to it.  just like how we deal with any other state in elixir.
# to render, we need a view.  pageview is like a view controller in other languages.
# could add extra code to pageview.  anyway once it's done the webview, it'll look for the appropriate template.
# where to put helper functions used by the template, e.g. our plural_of() function?  well it's a helper for the view, so it should go into view/page_view.ex
# controllers/page_controller.ex handles specific requests
# endpoint.ex is the entry point for communications
# router.ex is for sending requests to the correct controller/function
# templates/layout/app.html.eex is the app-wide web page layout
# templates/page/index.html.eex is the per-request page layout
# views/page_view.ex holds support code for templates

# EEx
# .eex is embedded elixir.  it's a preprocessor that looks for <% ... %> in text.  EEx comes standard with elixir, don't need phoenix to run it.
# most common use is to evaluate an elixir expression and put result back into original string.  this uses the <%= form of EEx substitution.
# the equals sign will always inject a value back into the original string.
# the only variables available while executing the code in templates are those that are passed to it with =.  or with assigns and @, which is what phoenix uses.
# keep in mind that normally EEx just replaces stuff between <% and %>.  newlines after the expression are easily forgotten and left in.
# or just use trim mode:  EEx.eval_string(template, [], trim: true) (although this doesn't seem to do anything for me...)

# put view code into helper functions under the views/ directory.  a naming convention links controllers, views, and templates.
# for example, if AbcController is handling a request, and it renders xyz.html, then the view logic will be handled by views/abc_view and the template in templates/abc/xyz.html.eex will be rendered.
# those are defaults, and can be overriden.

# helper functions can be called using its fully-qualified name from any view.
# you can also import modules into any view.

# since text returned by a helper will be escaped for safety, you can override this behavior by wrapping the string in a tuple:  {:safe, "<span style='color: blue'>#{val}</span>"}

# the default layout for an app is under templates/layout/app.html.eex.  but this can be overridden for particular controllers or controller functions.
# stylesheets are in assets/css.  app-specific styles are in assets/css/app.css.  results are cat'd into priv/static/css/app.css.
# js files are built from assets/js/app.js.
# babel is run on all js outside the vendor tree
# no css preprocessing is applied by default
# all files in assets/static are copied to priv/static

# in order to reference a static asset, you need to use a built-in helper function Routes.static_path() that creates the asset path.

# assigns is a list of values that the controller wants to make available to the view.
# assigns in a view start with @.

# think of phoenix not as a web framework or RoR but rather as a switch:  it manipulates incoming traffic, packages it in an appropriate format, and switches it across to an appropriate handler that can do whatever it has to do, and then maybe sends a response.
# phoenix is very performant, can handle millions of connections on a single machine.
# for an incoming request, phoenix will:
# add request ID
# log raw request
# parse content
# determine verb
# decode session information
# validate user
# verify access
# maybe serve from cache
# route to application, which our code then handles
# then formats a response.
# each of these is performed by a plug.  a plug is a function that takes a connection and a set of options, then returns an updated connection.  so it's basically a reducer.

# phoenix stores sessions in encrypted cookies on the user's browser, so we can create a session and set something in it and the session will be passed back to server via the cookie and made available in the connection object.
# sessions represent a history of interactions for a particular browser.  simple key-value store.
# put_session(conn, key, value) and get_session(conn, key)
# path helpers are automatically defined on the <our app name>.Router.Helpers module, which is aliased to Routes:  https://hexdocs.pm/phoenix/routing.html#path-helpers

# keys of key-value pairs passed to render() become assigns

# in general, avoid coding html cuz there's likely a helper that can do it for you.
# usually, anything more complex than a single statement doesn't belong in a template.
# helpers can be put anywhere and shared between templates, even though by a default a view is created for each controller and functions defined for that view are available to all templates for that controller.
# so a controller has a view and templates

# the controller function invoked by an incoming request depends on BOTH the incoming path AND the http verb.  hence you can do a GET and POST to the same URL.

# form data can be stored in the connection, or it can be stored in a change set for a database that's used directly in our app, which is our ecto represents database alterations.

# http PUT is used for updating an existing resource on the server (in our case, the game state)
