defmodule Procs do

    def greeter(count) do
        receive do
            {:boom, reason} ->
                exit(reason)
            :reset ->
                greeter(0)
            {:add, n} ->
                greeter(count + n)
            message ->
                IO.puts "#{count}:  Hello #{inspect message}"
                greeter(count)
        end
    end

end
