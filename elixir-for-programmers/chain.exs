defmodule Chain do

    defstruct(
        next_node: nil,
        count: 100000
    )

    def start_link(next_node) do
        spawn_link(Chain, :message_loop, [%Chain{next_node: next_node}])
        |> Process.register(:chainer)  # recall this name is local to calling node
    end

    def message_loop(%{count: 0}) do
        IO.puts "done"
    end

    def message_loop(state) do  # state is the argument for message_loop() that was provided to spawn_link
        receive do
            {:trigger} ->
                send({:chainer, state.next_node}, {:trigger})
        end
        message_loop(%{state | count: state.count - 1})
    end

end
