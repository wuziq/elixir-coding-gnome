defmodule Fib do

    alias Fib.Impl

    defdelegate fib(num), to: Impl

end
