defmodule Fib.Impl do

    alias Cache.Cache

    # get its first fib value from cache
        # if it doesn't exist:
            # calculate the fib value:
                # get its first fib value from cache
                    # if it doesn't exist:
                        # calculate the fib value
                        # add it to the cache
                    # else, return it
                # get its second fib value from cache
                    # if it doesn't exist:
                        # calculate the fib value:
                        # add it to the cache
                    # else, return it
                # add them together and return result
            # add it to the cache
        # else, return it
    # get its second fib value from cache

    def fib(num) do
        Cache.get(num)
        |> calculate_or_return(num)
    end

    defp calculate_or_return(:nil, num) do
        fib_value = fib(num - 1) + fib(num - 2)
        Cache.add(num, fib_value)
        fib_value
    end

    defp calculate_or_return(val, _num) do
        val
    end

end
